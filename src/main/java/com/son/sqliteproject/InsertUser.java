/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.son.sqliteproject;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

/**
 *
 * @author mc-so
 */
public class InsertUser {
     public static void main(String[] args) {
        Connection conn = null;
        String dbName = "user.db";
        Statement stmt = null;

        try {
            Class.forName("org.sqlite.JDBC");
            conn = DriverManager.getConnection("jdbc:sqlite:" + dbName);
            conn.setAutoCommit(false);
            stmt = conn.createStatement();

            String sql = "INSERT INTO USER (USERNAME,PASSWORD) "
                    + "VALUES ('Sunny','123456');";
            stmt.executeUpdate(sql);

            sql = "INSERT INTO USER (USERNAME,PASSWORD) "
                    + "VALUES ('Sumo','123456');";
            stmt.executeUpdate(sql);
            conn.commit();
            stmt.close();
            
            conn.close();
        } catch (ClassNotFoundException ex) {
            System.out.println("Libary org.sqlite.JDBC not found!!");
            System.exit(0);
        } catch (SQLException ex) {
            System.out.println("Unable to open database");
            System.exit(0);
        }
        System.out.println("Opened database successfully");
    }
}
